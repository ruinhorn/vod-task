package com.example.vod_telecom.models

import android.os.Parcelable

interface PlayerData : Parcelable {

    fun provideVideoUrl(): String

    fun provideTitle(): String
}