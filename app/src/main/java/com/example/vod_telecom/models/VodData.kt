package com.example.vod_telecom.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.Date

sealed class VodData : Parcelable {
    abstract val id: Int
    abstract val isAdult: Boolean
    abstract val genres: List<String>
    abstract val originalLanguage: String
    abstract val originalTitle: String
    abstract val overview: String
    abstract val releaseDate: Date
    abstract val posterUrl: String
    abstract val popularity: Float
    abstract val title: String
    abstract val videoUrl: String
    abstract val voteAverage: Float
    abstract val voteCount: Int

    @Parcelize
    data class Movie(
        override val isAdult: Boolean,
        override val id: Int,
        override val genres: List<String>,
        override val originalLanguage: String,
        override val originalTitle: String,
        override val overview: String,
        override val releaseDate: Date,
        override val posterUrl: String,
        override val popularity: Float,
        override val title: String,
        override val videoUrl: String,
        override val voteAverage: Float,
        override val voteCount: Int
    ) : VodData(), PlayerData {

        override fun provideTitle() = title

        override fun provideVideoUrl() = videoUrl
    }
}