package com.example.vod_telecom.data

import com.example.vod_telecom.data.vod.models.VodResponse
import com.example.vod_telecom.models.VodData
import java.text.SimpleDateFormat
import java.util.Locale

private const val ISO_8601_DATE = "yyyy-MM-dd"

fun VodResponse.toVodDataList(): List<VodData> = results.map {
    VodData.Movie(
        it.adult,
        it.id,
        it.genres.map { genre -> genre.name },
        it.originalLanguage,
        it.originalTitle,
        it.overview,
        it.releaseDate.parseDate(ISO_8601_DATE),
        it.posterUrl,
        it.popularity,
        it.title,
        it.videoUrl,
        it.voteAverage,
        it.voteCount
    )
}

fun String.parseDate(pattern: String) = SimpleDateFormat(pattern, Locale.getDefault()).parse(this)
