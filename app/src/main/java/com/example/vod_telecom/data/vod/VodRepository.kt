package com.example.vod_telecom.data.vod

import com.example.vod_telecom.data.vod.models.VodResponse
import io.reactivex.Observable

interface VodRepository {

    fun getVodData(): Observable<VodResponse>
}