package com.example.vod_telecom.data.vod


class VodRepositoryImpl(private val remoteSource: VodRemoteSource) :
    VodRepository {

    override fun getVodData() = remoteSource.request()
}