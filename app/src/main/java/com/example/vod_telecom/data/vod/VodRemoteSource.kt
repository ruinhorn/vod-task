package com.example.vod_telecom.data.vod

import com.example.vod_telecom.data.api.VodApi

class VodRemoteSource(private val api: VodApi) {

    fun request() = api.getVodData()
}