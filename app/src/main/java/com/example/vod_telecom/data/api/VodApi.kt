package com.example.vod_telecom.data.api

import com.example.vod_telecom.data.vod.models.VodResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface VodApi {

    @GET("movies.json")
    fun getVodData(): Observable<VodResponse>
}