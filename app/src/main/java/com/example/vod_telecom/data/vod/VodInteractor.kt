package com.example.vod_telecom.data.vod

import com.example.vod_telecom.data.toVodDataList
import com.example.vod_telecom.models.VodData
import io.reactivex.Observable

class VodInteractor(private val repository: VodRepository) : () -> Observable<List<VodData>> {

    override fun invoke() = repository.getVodData().map { it.toVodDataList() }
}