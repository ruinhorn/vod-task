package com.example.vod_telecom.data.vod.models

import kotlinx.serialization.Serializable

@Serializable
data class VodResponseGenre(
    val name: String
)