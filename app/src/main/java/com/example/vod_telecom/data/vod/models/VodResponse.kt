package com.example.vod_telecom.data.vod.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VodResponse(
    val page: Int,
    val results: List<VodResponseItem>,
    @SerialName("total_pages")
    val totalPages: Int,
    @SerialName("total_results")
    val totalResults: Int
)