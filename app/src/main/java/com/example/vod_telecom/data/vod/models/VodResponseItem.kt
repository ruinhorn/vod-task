package com.example.vod_telecom.data.vod.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class VodResponseItem(
    val adult: Boolean,
    val genres: List<VodResponseGenre>,
    val id: Int,
    @SerialName("original_language")
    val originalLanguage: String,
    @SerialName("original_title")
    val originalTitle: String,
    val overview: String,
    @SerialName("release_date")
    val releaseDate: String,
    @SerialName("poster_path")
    val posterUrl: String,
    val popularity: Float,
    val title: String,
    @SerialName("video")
    val videoUrl: String,
    @SerialName("vote_average")
    val voteAverage: Float,
    @SerialName("vote_count")
    val voteCount: Int
)