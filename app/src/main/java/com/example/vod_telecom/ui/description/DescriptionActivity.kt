package com.example.vod_telecom.ui.description

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.vod_telecom.models.VodData

class DescriptionActivity : AppCompatActivity() {

    companion object {
        const val VOD_KEY = "VOD_KEY"

        fun start(context: Context, vodData: VodData) {
            val intent = Intent(context, DescriptionActivity::class.java)
            intent.putExtra(VOD_KEY, vodData)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(
                android.R.id.content, DescriptionFragment.newInstance(
                    intent.getParcelableExtra(VOD_KEY)
                )
            ).commit()
        }
    }
}