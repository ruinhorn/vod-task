package com.example.vod_telecom.ui.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.vod_telecom.R
import com.example.vod_telecom.models.PlayerData
import com.example.vod_telecom.ui.player.PlayerActivity.Companion.PLAYER_DATA_KEY
import com.google.android.exoplayer2.ExoPlayer
import kotlinx.android.synthetic.main.fmt_player.playerView
import kotlinx.android.synthetic.main.item_error.error
import kotlinx.android.synthetic.main.item_error.errorText
import kotlinx.android.synthetic.main.item_throbber.throbber


class PlayerFragment : MvpAppCompatFragment(), PlayerView {

    companion object {

        fun newInstance(playerData: PlayerData) = PlayerFragment().apply {
            arguments = Bundle().apply { putParcelable(PLAYER_DATA_KEY, playerData) }
        }
    }

    @InjectPresenter(type = PresenterType.LOCAL)
    lateinit var presenter: PlayerPresenter

    private val playerData: PlayerData by lazy {
        arguments?.getParcelable<PlayerData>(PLAYER_DATA_KEY) ?: throw PlayerDataNotPassed()
    }

    @ProvidePresenter
    fun provideDetailsPresenter() =
        PlayerPresenter(playerData)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fmt_player, container, false)

    override fun setPlayer(player: ExoPlayer) {
        playerView.player = player
    }

    override fun showError(message: String) {
        error.visibility = View.VISIBLE
        errorText.text = message
    }

    override fun onResume() {
        super.onResume()
        presenter.play()
    }

    override fun onStop() {
        super.onStop()
        presenter.pause()
    }

    override fun showLoading(isLoading: Boolean) {
        if (isLoading) {
            throbber.visibility = View.VISIBLE
        } else {
            throbber.visibility = View.GONE
        }
    }
}

private class PlayerDataNotPassed(message: String = "PlayerData not passed") : Exception(message)
