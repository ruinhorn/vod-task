package com.example.vod_telecom.ui.player

import com.arellomobile.mvp.MvpView
import com.google.android.exoplayer2.ExoPlayer

interface PlayerView : MvpView {

    fun setPlayer(player: ExoPlayer)

    fun showLoading(isLoading: Boolean)

    fun showError(message: String)
}