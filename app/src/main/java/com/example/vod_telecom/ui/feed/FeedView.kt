package com.example.vod_telecom.ui.feed

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.vod_telecom.models.VodData

@StateStrategyType(AddToEndSingleStrategy::class)
interface FeedView : MvpView {

    fun showFeed(feed: List<VodData>)

    fun showLoader()

    fun hideLoader()

    fun showError(errorMessage: String)

    fun hideError()
}