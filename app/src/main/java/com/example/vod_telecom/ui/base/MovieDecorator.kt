package com.example.vod_telecom.ui.base

import android.graphics.Rect
import android.support.annotation.DimenRes
import android.support.v7.widget.RecyclerView
import android.view.View

class MovieDecorator(
    @DimenRes private val spacingDp: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        view.resources.getDimensionPixelSize(spacingDp).let {
            outRect.set(it, it, it, it)
        }
    }
}