package com.example.vod_telecom.ui.player

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.vod_telecom.models.PlayerData

class PlayerActivity : AppCompatActivity() {

    companion object {
        const val PLAYER_DATA_KEY = "PLAYER_DATA_KEY"

        fun start(context: Context, playerData: PlayerData) {
            Intent(context, PlayerActivity::class.java).also {
                it.putExtra(PLAYER_DATA_KEY, playerData)
                context.startActivity(it)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(
                    android.R.id.content, PlayerFragment.newInstance(
                        intent.getParcelableExtra(
                            PLAYER_DATA_KEY
                        )
                    )
                ).commit()
        }
    }
}