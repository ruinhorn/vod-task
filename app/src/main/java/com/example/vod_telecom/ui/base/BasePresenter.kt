package com.example.vod_telecom.ui.base

import android.support.annotation.CallSuper
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent

abstract class BasePresenter<VIEW : MvpView>(
    protected val mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread(),
    protected val workThreadScheduler: Scheduler = Schedulers.io()
) : MvpPresenter<VIEW>(), KoinComponent {

    private val compositeDisposable = CompositeDisposable()

    protected fun <T> Observable<T>.addSubscription(
        onComplete: (() -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null,
        onNext: ((T) -> Unit)? = null
    ) = this.subscribeOn(workThreadScheduler)
        .observeOn(mainThreadScheduler)
        .subscribe({ onNext?.invoke(it) }, { onError?.invoke(it) }, { onComplete?.invoke() })
        .let(compositeDisposable::add)

    protected fun clearDisposable() {
        compositeDisposable.clear()
    }

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}