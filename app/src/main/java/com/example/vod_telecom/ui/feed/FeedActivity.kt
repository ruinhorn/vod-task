package com.example.vod_telecom.ui.feed

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class FeedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(android.R.id.content, FeedFragment())
                .commit()
        }
    }
}