package com.example.vod_telecom.ui.player

import com.arellomobile.mvp.InjectViewState
import com.example.vod_telecom.models.PlayerData
import com.example.vod_telecom.ui.base.BasePresenter
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.inject

@InjectViewState
class PlayerPresenter(
    mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread(),
    workThreadScheduler: Scheduler = Schedulers.io()
) : BasePresenter<PlayerView>(mainThreadScheduler, workThreadScheduler) {

    constructor(
        playerData: PlayerData, mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread(),
        workThreadScheduler: Scheduler = Schedulers.io()
    ) : this(mainThreadScheduler, workThreadScheduler) {
        this.playerData = playerData
    }

    private var playerData: PlayerData? = null
    private val mediaPlayer: PlayerDelegate by inject()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mediaPlayer.preparePlayer(playerData?.provideVideoUrl() ?: "")
        mediaPlayer.setStateListener(object : Player.EventListener {
            override fun onLoadingChanged(isLoading: Boolean) {
                viewState.showLoading(isLoading)
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                viewState.showError(error?.message ?: "")
            }
        })
        viewState.setPlayer(mediaPlayer.getPlayerImpl())
    }

    fun play() {
        if (mediaPlayer.getState() == Player.STATE_ENDED) {
            mediaPlayer.seekTo(0)
        }
        mediaPlayer.play()
    }

    fun pause() {
        mediaPlayer.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.removeListeners()
        mediaPlayer.releasePlayer()
    }
}