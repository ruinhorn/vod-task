package com.example.vod_telecom.ui.feed

import android.support.annotation.VisibleForTesting
import com.arellomobile.mvp.InjectViewState
import com.example.vod_telecom.data.vod.VodInteractor
import com.example.vod_telecom.ui.base.BasePresenter
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.inject

@InjectViewState
class FeedPresenter(
    mainThreadScheduler: Scheduler = AndroidSchedulers.mainThread(),
    workThreadScheduler: Scheduler = Schedulers.io()
) : BasePresenter<FeedView>(mainThreadScheduler, workThreadScheduler) {

    private val vodInteractor: VodInteractor by inject()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        load()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun load() {
        viewState.showLoader()
        vodInteractor()
            .doOnError {
                viewState.hideLoader()
                viewState.showError(it.message ?: "")
            }
            .addSubscription {
                viewState.hideLoader()
                viewState.showFeed(it)
            }
    }
}