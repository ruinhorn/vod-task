package com.example.vod_telecom.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.vod_telecom.R
import com.example.vod_telecom.models.VodData
import com.example.vod_telecom.ui.base.inflate
import com.example.vod_telecom.ui.base.loadImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_movie.poster
import kotlinx.android.synthetic.main.item_movie.title

class VodAdapter(
    private val onItemClicked: (VodData) -> Unit
) : RecyclerView.Adapter<VodAdapter.VodViewHolder>() {

    private var vodList: MutableList<VodData> = mutableListOf()

    fun appendData(list: List<VodData>) {
        val size = vodList.size
        vodList.addAll(list)
        notifyItemRangeInserted(size, list.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VodViewHolder(
        parent.inflate(R.layout.item_movie),
        onItemClicked
    )

    override fun getItemCount() = vodList.size

    override fun onBindViewHolder(holder: VodViewHolder, position: Int) {
        holder.bind(vodList[position])
    }

    class VodViewHolder(itemView: View, private val onItemClicked: (VodData) -> Unit) :
        RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView = itemView

        fun bind(vodData: VodData) {
            poster.loadImage(vodData.posterUrl)
            title.text = vodData.title
            itemView.setOnClickListener {
                onItemClicked(vodData)
            }
        }
    }
}