package com.example.vod_telecom.ui.player

import android.net.Uri
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import org.koin.core.KoinComponent

class PlayerDelegateImpl(
    private val exoPlayer: ExoPlayer,
    private val dataSourceFactory: DefaultDataSourceFactory
) :
    PlayerDelegate, KoinComponent {

    private val listeners: MutableSet<Player.EventListener> = mutableSetOf()

    override fun preparePlayer(url: String) {
        val mediaSource = ExtractorMediaSource
            .Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(url))
        exoPlayer.prepare(mediaSource)
    }

    override fun getPlayerImpl() = exoPlayer

    override fun play() {
        exoPlayer.playWhenReady = true
    }

    override fun pause() {
        exoPlayer.playWhenReady = false
    }

    override fun releasePlayer() {
        exoPlayer.stop()
        exoPlayer.release()
    }

    override fun getState() = exoPlayer.playbackState

    override fun setStateListener(listener: Player.EventListener) {
        exoPlayer.addListener(listener)
        listeners.add(listener)
    }

    override fun removeListeners() {
        listeners.forEach {
            exoPlayer.removeListener(it)
        }
    }

    override fun seekTo(position: Long) {
        exoPlayer.seekTo(position)
    }
}
