package com.example.vod_telecom.ui.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.vod_telecom.R
import com.example.vod_telecom.models.VodData
import com.example.vod_telecom.ui.VodAdapter
import com.example.vod_telecom.ui.base.MovieDecorator
import com.example.vod_telecom.ui.description.DescriptionActivity
import kotlinx.android.synthetic.main.fmt_feed.recyclerView
import kotlinx.android.synthetic.main.item_error.error
import kotlinx.android.synthetic.main.item_error.errorText
import kotlinx.android.synthetic.main.item_throbber.throbber

class FeedFragment : MvpAppCompatFragment(), FeedView {

    @InjectPresenter
    lateinit var feedPresenter: FeedPresenter

    private val adapter by lazy { VodAdapter(::onItemClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fmt_feed, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.addItemDecoration(MovieDecorator(R.dimen.movie_item_margin))
        recyclerView.adapter = adapter
    }

    private fun onItemClicked(vodData: VodData) {
        context?.let { DescriptionActivity.start(it, vodData) } ?: return
    }

    override fun showFeed(feed: List<VodData>) {
        adapter.appendData(feed)
    }

    override fun showLoader() {
        throbber.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        throbber.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        errorText.text = errorMessage
        error.visibility = View.VISIBLE
    }

    override fun hideError() {
        error.visibility = View.GONE
    }
}
