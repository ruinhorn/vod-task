package com.example.vod_telecom.ui.description

import android.os.Bundle
import android.support.design.chip.Chip
import android.support.design.widget.AppBarLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.vod_telecom.R
import com.example.vod_telecom.models.PlayerData
import com.example.vod_telecom.models.VodData
import com.example.vod_telecom.ui.base.loadImage
import com.example.vod_telecom.ui.description.DescriptionActivity.Companion.VOD_KEY
import com.example.vod_telecom.ui.player.PlayerActivity
import kotlinx.android.synthetic.main.fmt_description.appBar
import kotlinx.android.synthetic.main.fmt_description.collapsingToolbar
import kotlinx.android.synthetic.main.fmt_description.expandedTitle
import kotlinx.android.synthetic.main.fmt_description.genresChips
import kotlinx.android.synthetic.main.fmt_description.languageChips
import kotlinx.android.synthetic.main.fmt_description.overview
import kotlinx.android.synthetic.main.fmt_description.play_button
import kotlinx.android.synthetic.main.fmt_description.play_fab
import kotlinx.android.synthetic.main.fmt_description.rating
import kotlinx.android.synthetic.main.fmt_description.rentalDate
import kotlinx.android.synthetic.main.fmt_description.toolbarImage
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DescriptionFragment : MvpAppCompatFragment() {

    companion object {
        private const val DATE_FORMAT = "dd MMMM yyyy"

        fun newInstance(vodData: VodData) = DescriptionFragment().apply {
            arguments = Bundle().apply { putParcelable(VOD_KEY, vodData) }
        }
    }

    private val vodData: VodData by lazy {
        arguments?.getParcelable<VodData>(VOD_KEY) ?: throw DataNotPassed()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fmt_description, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarImage.loadImage(vodData.posterUrl)
        appBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            val percent = (Math.abs(verticalOffset).toFloat() / appBar.totalScrollRange)
            play_fab.alpha = percent
        })
        setupView()
    }

    private fun setupView() {
        collapsingToolbar.title = vodData.title
        expandedTitle.text = vodData.title
        rentalDate.text = formatDate(vodData.releaseDate)
        overview.text = vodData.overview
        rating.text = vodData.voteAverage.toInt().toString()
        Chip(genresChips.context).apply {
            text = vodData.originalLanguage
            languageChips.addView(this)
            isClickable = false
        }
        vodData.genres.forEach {
            Chip(genresChips.context).apply {
                text = it
                genresChips.addView(this)
                isClickable = false
            }
        }
        play_button.setOnClickListener {
            onPlayClicked()
        }
        play_fab.setOnClickListener {
            onPlayClicked()
        }
    }

    private fun formatDate(date: Date) =
        SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(date)

    private fun onPlayClicked() {
        PlayerActivity.start(context ?: return, vodData as PlayerData)
    }
}

private class DataNotPassed(message: String = "Provide correct VOD data") : Exception(message)