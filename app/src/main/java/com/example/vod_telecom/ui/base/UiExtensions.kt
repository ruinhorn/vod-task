package com.example.vod_telecom.ui.base

import android.content.Context
import android.content.res.Resources
import android.support.annotation.DimenRes
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.vod_telecom.R

private fun Context.buildGlideRequest(src: String?) =
    GlideApp.with(this)
        .load(src)

fun ImageView.loadImage(src: String?, @DrawableRes errorRes: Int = 0) {
    context.buildGlideRequest(src)
        .error(errorRes)
        .into(this)
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

//calculates span count based on view width, but hardcoded now.
fun Resources.calculateSpanCount(@DimenRes widthDp: Int): Int {
    val widthPx = getDimensionPixelSize(widthDp)
    val displayWidthPx = displayMetrics.widthPixels
    val gridPaddingPx = getDimensionPixelSize(R.dimen.movie_item_margin)
    return ((displayWidthPx - gridPaddingPx) / (widthPx + gridPaddingPx))
}