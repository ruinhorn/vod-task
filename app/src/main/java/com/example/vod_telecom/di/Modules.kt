package com.example.vod_telecom.di

import com.example.vod_telecom.R
import com.example.vod_telecom.data.api.VodApi
import com.example.vod_telecom.data.vod.VodInteractor
import com.example.vod_telecom.data.vod.VodRemoteSource
import com.example.vod_telecom.data.vod.VodRepository
import com.example.vod_telecom.data.vod.VodRepositoryImpl
import com.example.vod_telecom.ui.player.PlayerDelegate
import com.example.vod_telecom.ui.player.PlayerDelegateImpl
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

private const val BASE_URL =
    "https://gist.githubusercontent.com/LukyanovAnatoliy/eca5141dedc79751b3d0b339649e06a3/raw/38f9419762adf27c34a3f052733b296385b6aa8d/"
private const val MEDIA_TYPE = "application/json"
private const val USER_AGENT = "USER_AGENT"

val appModule = module {

    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(get())
            .addConverterFactory(Json.nonstrict.asConverterFactory(MediaType.get(MEDIA_TYPE)))
            .build()
            .create(VodApi::class.java)
    }

    single { VodRemoteSource(get()) }

    single { VodRepositoryImpl(get()) as VodRepository }

    single { VodInteractor(get()) }
}

val playerModule = module {

    single(named(USER_AGENT)) {
        Util.getUserAgent(
            get(),
            androidContext().getString(R.string.app_name)
        )
    }

    single { DefaultDataSourceFactory(get(), get<String>(named(USER_AGENT))) }

    factory {
        ExoPlayerFactory.newSimpleInstance(
            androidContext(),
            DefaultRenderersFactory(androidContext()),
            DefaultTrackSelector(),
            DefaultLoadControl()
        ) as ExoPlayer
    }

    factory {
        PlayerDelegateImpl(get(), get()) as PlayerDelegate
    }
}