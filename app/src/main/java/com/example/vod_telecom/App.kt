package com.example.vod_telecom

import android.app.Application
import com.example.vod_telecom.di.appModule
import com.example.vod_telecom.di.playerModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(appModule, playerModule)
        }
    }
}