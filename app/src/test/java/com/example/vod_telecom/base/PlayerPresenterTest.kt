package com.example.vod_telecom.base

import android.os.Parcel
import com.example.vod_telecom.models.PlayerData
import com.example.vod_telecom.ui.player.PlayerDelegate
import com.example.vod_telecom.ui.player.PlayerPresenter
import com.example.vod_telecom.ui.player.PlayerView
import com.example.vod_telecom.ui.player.`PlayerView$$State`
import org.junit.Test
import org.koin.core.qualifier.named
import org.koin.test.inject
import org.koin.test.mock.declare
import org.koin.test.mock.declareMock
import org.mockito.Mock
import org.mockito.Mockito

class PlayerPresenterTest : BasePresenterTest() {

    @Mock
    lateinit var view: PlayerView

    @Mock
    lateinit var viewState: `PlayerView$$State`

    private val presenter by lazy {
        PlayerPresenter(mockPlayerData(), getTestScheduler(), getTestScheduler())
    }

    private val playerDelegate: PlayerDelegate by inject()

    override fun before() {
        super.before()
        declare {
            single(named("USER_AGENT")) { "TEST" }
        }
        declareMock<PlayerDelegate>()
    }

    @Test
    fun `positive scenario player set`() {
        presenter.attachView(view)
        presenter.setViewState(viewState)
        playerDelegate.preparePlayer("TEST")
        Mockito.verify(view).setPlayer(playerDelegate.getPlayerImpl())
    }

    private fun mockPlayerData() = object : PlayerData {
        override fun writeToParcel(dest: Parcel?, flags: Int) {
        }

        override fun describeContents() = 0

        override fun provideVideoUrl() = "TEST"

        override fun provideTitle() = "TEST"

    }
}