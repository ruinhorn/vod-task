package com.example.vod_telecom.base

import android.support.annotation.CallSuper
import com.example.vod_telecom.di.appModule
import com.example.vod_telecom.di.playerModule
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
abstract class BasePresenterTest : AutoCloseKoinTest() {

    @Before
    @CallSuper
    open fun before() {
        MockitoAnnotations.initMocks(this)
        startKoin {
            modules(appModule, playerModule)
        }
    }

    fun getTestScheduler() = Schedulers.trampoline()
}