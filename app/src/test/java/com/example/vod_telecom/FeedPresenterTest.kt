package com.example.vod_telecom

import com.example.vod_telecom.base.BasePresenterTest
import com.example.vod_telecom.data.vod.VodInteractor
import com.example.vod_telecom.models.VodData
import com.example.vod_telecom.ui.feed.FeedPresenter
import com.example.vod_telecom.ui.feed.FeedView
import com.example.vod_telecom.ui.feed.`FeedView$$State`
import io.reactivex.Observable
import org.junit.Test
import org.koin.test.inject
import org.koin.test.mock.declareMock
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import java.util.Date

class FeedPresenterTest : BasePresenterTest() {

    @Mock
    lateinit var viewState: `FeedView$$State`

    @Mock
    lateinit var view: FeedView

    private val presenter by lazy {
        FeedPresenter(getTestScheduler(), getTestScheduler())
    }

    private val vodInteractor: VodInteractor by inject()

    override fun before() {
        super.before()
        declareMock<VodInteractor>()
    }

    @Test
    fun `positive scenario data loaded`() {
        mockVodInteractor()
        presenter.attachView(view)
        presenter.setViewState(viewState)
        presenter.load()
        verify(view).showLoader()
        verify(view).hideLoader()
        verify(view).showFeed(getVodData())
    }

    @Test
    fun `negative scenario error during loading`() {
        mockVodError()
        presenter.attachView(view)
        presenter.setViewState(viewState)
        presenter.load()
        verify(view).showLoader()
        verify(view).hideLoader()
        verify(view).showError("TEST")
    }

    fun mockVodError() {
        Mockito.`when`(vodInteractor()).thenReturn(
            Observable.error(Exception("TEST"))
        )
    }

    fun mockVodInteractor() {
        Mockito.`when`(vodInteractor()).thenReturn(
            Observable.just(getVodData())
        )
    }

    fun getVodData() = listOf(
        VodData.Movie(
            false,
            0,
            listOf("TEST"),
            "TEST",
            "TEST",
            "TEST",
            Date(1558302314),
            "TEST",
            0f,
            "TEST",
            "TEST",
            0f,
            0
        )
    )
}
